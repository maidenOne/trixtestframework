# Echo client program
import socket
import time
import sys
import json
import os

def _send(socket, data):
  try:
    serialized = str(json.dumps(data))
  except (TypeError, ValueError):
    raise Exception('You can only send JSON-serializable data')
  # send the length of the serialized data first
  #print("send1",'%d\n'.encode() % len(serialized))
  socket.send('%d\n'.encode() % len(serialized))
  # send the serialized data
  #print("send2",serialized.encode())
  socket.sendall(serialized.encode())
  #print("sent")

def _recv(socket):
  # read the length of the data, letter by letter until we reach EOL
  length_str = ''
  char = socket.recv(1).decode("utf-8")
  #print("got:",char)
  while char != '\n':
    #print("got: ", char)
    length_str += char
    char = socket.recv(1).decode("utf-8")
  total = int(length_str)
  # use a memoryview to receive the data chunk by chunk efficiently
  view = memoryview(bytearray(total))
  next_offset = 0
  while total - next_offset > 0:
    #print("got: ", total -next_offset)
    recv_size = socket.recv_into(view[next_offset:], total - next_offset)
    next_offset += recv_size
  try:
    deserialized = json.loads(view.tobytes())
  except (TypeError, ValueError):
    raise Exception('Data received was not in JSON format')
  return deserialized

class TrixAPI():
        def __init__(self,host='127.0.0.1',port='1988'):
                self.host = host
                self.port = port
                self.nodes = []
                self.tests = []
                self.getNodes()
                self.getTests()

        def sendRequest(self,request):
                for res in socket.getaddrinfo(self.host, self.port, socket.AF_UNSPEC, socket.SOCK_STREAM):
                        af, socktype, proto, canonname, sa = res
                        try:
                                s = socket.socket(af, socktype, proto)
                        except socket.error:
                                print('fail 2 connect')
                                s = None
                                continue
                        try:
                                s.connect(sa)
                        except socket.error:
                                print('fail 2 connect 2')
                                s.close()
                                s = None
                                continue
                        break

                if s is None:
                        print ('could not open socket')
                        sys.exit(1)

                _send(s,request)

                data = str(_recv(s))
                return data
                time.sleep(1)
                s.close()

        def getNodes(self):
                self.nodes = self.sendRequest('{"type":"request","id":"getNodes"}')
                self.nodes = json.loads(self.nodes)
                return self.nodes

        def getTests(self):
                self.tests = self.sendRequest('{"type":"request","id":"getTests"}')
                self.tests = json.loads(self.tests)
                self.tests = [ v for v in self.tests if not v.startswith('__') ]
                return self.tests

        # get items of log instances from node
        def getLogs(self):
                self.logs = self.sendRequest('{"type":"request","id":"getLogs"}')
                self.logs = json.loads(self.logs)
                #self.logs = eval(self.logs)
                self.logs.sort(key=lambda tup: tup[0])
                #print (self.logs)
                return self.logs

        def getLog(self,filename):
                self.log = self.sendRequest('{"type":"request","id":"getLogFile","file":"'+str(filename)+'"}')
                self.log = json.loads(self.log)
                return self.log

        def deleteLog(self,filename):
                self.log = self.sendRequest('{"type":"request","id":"deleteLogFile","file":"'+str(filename)+'"}')
                return self.log

        # send a file in hex format to target and run test
        def refresh(self):
                self.sendRequest('{"type":"request","id":"refresh","target":"ALL"}')

        # send a file in hex format to target and run test
        def runTestOnNodeWithFwHex(self,testName,target,fwFileName,fwFileData):
                self.sendRequest('{"type":"request","id":"runTests","target":"'+target+'","test":"'+testName+'","fwName":"'+fwFileName+'","fwData":"'+fwFileData+'"}')

        # send a file from disk to target and run test
        def runTestOnNodeFilePath(self,test,target,file):
                (filepath, filename) = os.path.split(file)
                with open(file, "rb") as f:
                        firmwareFile = f.read()
                        encodedFwFile = firmwareFile.hex()
                        f.close()
                        self.runTestOnNodeWithFwHex(test,target,filename,encodedFwFile)
