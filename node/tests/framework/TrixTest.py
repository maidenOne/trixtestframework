import time, serial, sys, threading
import sched, time, datetime
import subprocess, signal
import importlib
import random

import interface.hal as hal

import asyncio
import sys
from asyncio.subprocess import PIPE, STDOUT

global_isPI = True

try:
	import RPi.GPIO as GPIO
except:
	global_isPI = False

global_testIsDone = False


class TrixTestFramework():
	def __init__(self):
		importlib.reload(sys)
		self.scheduler = sched.scheduler(time.time, time.sleep)
		self.powerLog = []
		self.debugLog = []
		self.testRunTime = None
		self.runTime = 0
		self.testTrace = []
		self.platform = 'test'
		self.fwFile = None
		self.testIsDone = False
		self.startTime = 0
		self.endTime = 0
		self.hal = hal.HAL()

		if sys.platform == "win32":
			self.loop = asyncio.ProactorEventLoop() # For subprocess' pipes on Windows
			asyncio.set_event_loop(loop)
		else:
			self.loop = asyncio.get_event_loop()

	def getSerial(self):
		global global_testIsDone
		for log in self.hal.readDebug():
			if global_testIsDone:
				break
			#print('debug',log)
			self.logDebug(log.decode('utf-8').strip())

	def getPower(self):
		global global_testIsDone
		for log in self.hal.readPower():
			if global_testIsDone:
				break
			#print('power',log)
			self.logPower(float(log))

	def execute(self,cmd):
		global global_testIsDone

		#popen = subprocess.Popen(cmd, stdout=subprocess.PIPE, universal_newlines=True, shell=True)
		popen = subprocess.Popen(cmd, stdout=subprocess.PIPE,shell=False)
		while True:
			data = popen.stdout.readline()
			if not data or global_testIsDone:
				if not data:
					print('no data from: ' + str(cmd))
				else:
					print('global_testIsDone is == true')
				popen.stdout.close()
				popen.send_signal(signal.SIGINT)
				time.sleep(0.1)
				popen.send_signal(signal.SIGQUIT)
				time.sleep(0.1)
				popen.send_signal(signal.SIGKILL)
				time.sleep(0.1)
				popen.terminate()
				break
			else:
				if(len(data)):
					yield data
		print ("Thread is terminating: " + str(cmd))

		popen.send_signal(signal.SIGINT)
		popen.send_signal(signal.SIGKILL)
		popen.stdout.close()
		popen.terminate()

	def setFwFile(self,file):
		self.fwFile = file

	def reset(self):
		self.hal.reset()

	def flash(self):
		global global_testIsDone
		global_testIsDone = False
		self.testIsDone = False
		self.trace('init logging')
		self.tLog = threading.Thread(target=self.getSerial)
		self.tPower = threading.Thread(target=self.getPower)
		print ("flash")
		self.trace('flash ' + str(self.fwFile))
		print('flashing: ' + str(self.fwFile))

		if(self.hal.flash(self.fwFile)):
			self.trace('FLASH SUCCESS')
		else:
			self.trace('FLASH FAILED')
		self.tLog.start()
		self.tPower.start()

	def trace(self,trace):
		self.testTrace.append([self.getTimestamp(),str(trace).encode('utf-8')])

	def resetTarget(self):
		self.trace('reset')
		self.hal.reset()

	def chargerOn(self):
		self.hal.chargerOn()
		self.trace('CHARGER ON')

	def chargerOff(self):
		self.hal.chargerOff()
		self.trace('CHARGER OFF')

	def chargerDuration(self,time):
		self.chargerOn()
		self.trace('WAIT',time)
		time.sleep(time)
		self.chargerOff()

	def buttonIn(self):
		self.trace('BUTTON IN')
		self.hal.buttonIn()

	def buttonOut(self):
		self.trace('buttonOut')
		self.hal.buttonOut()

	def buttonDuration(self,time):
		self.buttonIn()
		self.trace('WAIT',time)
		time.sleep(time)
		self.buttonOut()

	def getAveragePowerSinceBoot(self):
		self.trace('averagePowerSinceBoot')
		print('getavgpower1')
		#print (sum(float(i[1]) for i in self.powerLog))
		#if len(self.powerLog):
		#        print (len(self.powerLog))
		#        print (sum(float(i[1]) for i in self.powerLog) / len(self.powerLog))

	def getAveragePowerSinceTimeAgoSeconds(self,theTime):
		self.trace('averagePowerSinceTimeAgo')
		print('getavgpower2')
		newList = []
		timestamp = time.time() - float(theTime)
		#for a in self.powerLog:
		#	if float(a[0]) > timestamp:
		#		newList.append(a)
		#print sum(i[1] for i in self.powerLog)
		#if len(newList):
		#	print (len(newList))
		#	print (sum(float(i[1]) for i in newList) / len(newList))
		self.trace('done')

	def getTimestamp(self):
		return str(datetime.datetime.now()-self.startTime)
		#return "%.7f" % time.time()#time.strftime("%Y%m%d-%H%M%S")

	def logDebug(self,dbglog):
		message = str(dbglog).strip()
		if message:
			self.debugLog.append([self.getTimestamp(),message.strip()])

	def logPower(self,power):
		message = str(power).strip()
		if message:
			self.powerLog.append([self.getTimestamp(),float(message)])

	# will return number of instances that have the key
	def parse(self,keyToFind,expected):
		count = 0
		for s in self.debugLog:
			if keyToFind in s[1]:
				print ("found",keyToFind)
				count +=1
		if expected == None:
			return count
		if expected != count:
			raise Exception('PARSER: ' + str(keyToFind) + ' did occur: '+ str(count)+', expected: ' + str(expected))
		else:
			self.trace('PARSER:'+str(keyToFind)+' found '+ str(count) + 'times')

	def addEvent(self,time,check,params):
		self.scheduler.enter(time, 1, check, params)

	def start(self):
		global global_testIsDone
		print('start script')

		self.hal.start()

		self.trace('TEST_START')
		print('starting execution')
		self.tExecution = threading.Thread(target=self.executeTests)
		self.startTime = datetime.datetime.now()
		self.tExecution.start()

	def end(self):
		self.trace('TEST_END')
		self.hal.end()

	def cleanUpHw(self):
		global global_testIsDone
		global_testIsDone = True;
		# wait for threads to finish
		print ("clean up hw")
		print ("join execution")
		self.tExecution.join()
		self.hal.cleanDebug()
		self.hal.cleanPower()


	def executeTests(self):
		global global_testIsDone
		print('executing tests')
		try:
			self.scheduler.run()
		except Exception as e:
			print ("Exception: " + str(e))
			self.trace('EXCEPTION ' + str(e))
			self.trace('TEST ABORTED')
			time.sleep(0.5)
			global_testIsDone = True
			self.archive()
			return 1

		self.trace('TEST_SUCCESS')
		time.sleep(0.5) # to give threads time to upload before shutting down
		global_testIsDone = True
		self.testIsDone = True
		#self.archive()
		return 0

	def archive(self):
		print ('Store: ')
		#print ('Debug:')
		#for a in self.debugLog:
		#	print (str(a))
		#print ('Power:')
		#for a in self.powerLog:
		#	print (str(a))
		#print ('Trace')
		#for a in self.testTrace:
		#	print (str(a))

	def shutdown(self):
		self.loop.close()
