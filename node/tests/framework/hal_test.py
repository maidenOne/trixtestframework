import interface.hal
import threading, cmd, time

hal = interface.hal.HAL()

global_testIsDone = False

def getSerial():
    global global_testIsDone
    for log in hal.readDebug():
        if global_testIsDone:
            break
        print(log)

def getPower():
    global global_testIsDone
    for log in hal.readPower():
        if global_testIsDone:
            break
        print(log)

serial_thread = threading.Thread(target=getSerial)
power_thread = threading.Thread(target=getPower)
serial_thread.start()
power_thread.start()
time.sleep(1);
hal.cleanupDebug()
serial_thread.join()


time.sleep(1);
hal.cleanupPower()
power_thread.join()
