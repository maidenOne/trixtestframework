#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from time import sleep
import random, sys

try:
    while 1:
        print(str(random.randint(0,100)))
        sys.stdout.flush()
        sleep(0.1)


except KeyboardInterrupt:
    print ("\nCtrl-C pressed.  Program exiting...")
