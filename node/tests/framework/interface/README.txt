# Howto

1. run startJlink.sh, it will start the Jlink service that the rtt logger connects to

2. run flash.sh [file] it calls nrfjprg.sh that is a script that wraps flashing with Jlink tools

3. getSerial.sh starts JLinkRTTClient that fetches RTT log data through the JLink service

## charging

 charger[On/Off].py toggles pin 7 (gpio 4) to turn on or off the relay. (turning on/off the USB V+ for the charging pad)


