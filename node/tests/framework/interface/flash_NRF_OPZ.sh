#!/bin/sh

echo "setup flashing " $1
python interface/powerOn.py
stringName="program /root/node2/"$1" verify"
echo "init" > test.cfg
##echo "targets" >> test.cfg
##echo "reset init" >> test.cfg
echo "halt" >> test.cfg
echo "nrf51 mass_erase" >> test.cfg
echo "init" >> test.cfg
echo $stringName >> test.cfg
echo "reset run" >> test.cfg
#echo "shutdown" >> test.cfg
echo "exit" >> test.cfg

flash()
{
  echo "flashing"
  sudo openocd -f interface/flash.cfg -f test.cfg 2>&1
}

while : ; do
	flash
	if [ $? -eq 0 ]
	then
		break
	fi
	echo $?
done

echo "flashing done"
python interface/powerToggle.py 5.0
python interface/powerToggle.py 5.0
