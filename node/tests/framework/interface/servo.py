#!/usr/bin/env python

import RPi.GPIO as GPIO
import time

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(13,GPIO.OUT)

pwm=GPIO.PWM(13, 50)

def SetAngle(angle):
	duty = angle / 18 + 2
	GPIO.output(13, True)
	pwm.ChangeDutyCycle(duty)
	time.sleep(2)
	GPIO.output(13, False)
	pwm.ChangeDutyCycle(0)

pwm.start(0)
SetAngle(140)
