
import subprocess, signal

class HAL():
    def __init__(self):
        self.name = "lol"
        print('HAL INIT')
        self.debugPID = 0
        self.powerPID = 0

    def flash(self,filename):
        process = subprocess.Popen(['nrfjprog.sh','--flash',filename], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        #print(stdout)
        return True;

    def reset(self):
        pass

    def chargerOn(self):
        process = subprocess.Popen(['tests/framework/interface/chargerOn.py'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        #print(stdout)

    def chargerOff(self):
        process = subprocess.Popen(['tests/framework/interface/chargerOff.py'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        #print(stdout)

    def buttonIn(self):
        process = subprocess.Popen(['tests/framework/interface/buttonIn.py'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        #print(stdout)

    def buttonOut(self):
        process = subprocess.Popen(['tests/framework/interface/buttonOut.py'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        #print(stdout)

    def readDebug(self):
        popen = subprocess.Popen('JLinkRTTClient', stdout=subprocess.PIPE, shell=False)
        self.debugPID = popen.pid
        print('debug pid', self.debugPID)
        while True:
            try:
                data = popen.stdout.readline()
                if not data:
                    popen.terminate()
                    break
                else:
                    if(len(data)):
                        yield data
            except:
                print('log closed')
                return

    def cleanDebug(self):
        if(self.debugPID):
 #           print('clean debug pid', self.debugPID)
            subprocess.call(['kill',str(self.debugPID)])

    def readPower(self):
        popen = subprocess.Popen(['tests/framework/interface/getPower.py'], stdout=subprocess.PIPE, shell=False)
        self.powerPID = popen.pid
#        print('power pid', self.powerPID)
        while True:
            try:
                data = popen.stdout.readline()
                if not data:
                    print('fail, no data')
                    popen.terminate()
                    break
                else:
                    if(len(data)):
                        yield data
            except:
                print('log closed')
                return
        print ("power done")

    def cleanPower(self):
        if(self.powerPID):
            print('clean power pid', self.powerPID)
            subprocess.call(['kill',str(self.powerPID)])

    def start(self):
        print('start of test')
#        GPIO.setmode(GPIO.BOARD)
#        GPIO.setup(7,GPIO.OUT)
#        GPIO.setwarnings(False)
#        GPIO.setup(11,GPIO.OUT)
#        GPIO.output(7,1) # charging off
#        GPIO.output(11,0) # button out
#        GPIO.cleanup()

    def end(self):
        self.chargerOn()
        self.buttonOut()
        print('end of test')
#		GPIO.setwarnings(False)
#		GPIO.setmode(GPIO.BOARD)
#		GPIO.setup(7,GPIO.OUT)
#		GPIO.setup(11,GPIO.OUT)
#		GPIO.output(7,0)
#		GPIO.output(11,0)
