import sys
sys.path.append("tests/framework")
from TrixTest import TrixTestFramework

#raise ValueError('A very specific bad thing happened')
class Test(TrixTestFramework):
	def specialFunc(self):
		print ("i did")

	def run(self):
		self.flash()
		self.addEvent(1,self.chargerOn,())
		self.addEvent(3,self.chargerOff,())
		#self.addEvent(4,self.getAveragePowerSinceTimeAgoSeconds,(1,))
		#self.addEvent(5,self.getAveragePowerSinceBoot,())
		self.addEvent(10,self.end,())
		self.start()

#  0. set hardware in flash mode and flash
#  1. reset and start test from initial boot
#  2. verify power on button
#  3. verify led when not fully connected
#  4. verify charging led
#  5. verify stop changing led
#  6. verify led change when fix
#  7. verify power off and verify led
#  8. verify sleep
#  9. verify wakeup
# 10. verify firmware upgrade
