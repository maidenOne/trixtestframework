import sys
sys.path.append("tests/framework")
from TrixTest import TrixTestFramework

#raise ValueError('A very specific bad thing happened')
class Test(TrixTestFramework):
	def specialFunc(self):
		print ("i did")

	def run(self):
		self.flash()
		self.addEvent(1,self.chargerOn,())
		self.addEvent(3,self.chargerOff,())
		self.addEvent(60,self.end,())
		self.start()
