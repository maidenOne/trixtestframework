import sys
sys.path.append("tests/framework")
from TrixTest import TrixTestFramework

#raise ValueError('A very specific bad thing happened')
class Test(TrixTestFramework):
	def specialFunc(self):
		print ("i did")

	def secondsLater(self,time):
		self.timeSinceStart += time
		return self.timeSinceStart

	def addDelayedEvent(self,deltaTime,function,parameters):
		self.addEvent(self.secondsLater(deltaTime),function,parameters)

	def addDelayedParse(self,deltaTime,keyword,expected):
		self.addEvent(self.secondsLater(deltaTime),self.parse,(keyword,expected))

	def run(self):
		self.timeSinceStart = 0
		self.flash()
		# verify that modem was detected
#		self.addDelayedParse(20,"IAmNow",1)
		# run, then turn off'
		self.addDelayedEvent(20,self.buttonIn,())
		self.addDelayedEvent(5,self.buttonOut,())
#		self.addDelayedEvent(30,self.parse,("WillNowPowerOff",1))

		# verify that we do not boot when off and charging starts
		self.addDelayedEvent(10,self.chargerOn,())
#		self.addDelayedEvent(20,self.parse,("OFFcharging",1))

		# wait, then turn on again, verify that we remember our modem type
		self.addDelayedEvent(10,self.buttonIn,())
		self.addDelayedEvent(5,self.buttonOut,())
#		self.addDelayedEvent(30,self.parse,("IKnowIAmA",1))
		self.addDelayedEvent(10,self.end,())
		self.start()


#  0. set hardware in flash mode and flash
#  1. reset and start test from initial boot
#  2. verify power on button
#  3. verify led when not fully connected
#  4. verify charging led
#  5. verify stop changing led
#  6. verify led change when fix
#  7. verify power off and verify led
#  8. verify sleep
#  9. verify wakeup
# 10. verify firmware upgrade
