# Echo client program
import socket
import time
import sys
import json
import os
import importlib
import subprocess
from importlib import reload

time_limit = 30

def _send(socket, data):
  try:
    serialized = json.dumps(data)
  except:
    print ('fail to send, json?')
    raise Exception('You can only send JSON-serializable data')
  # send the length of the serialized data first
  try:
    #print('sending:',len(serialized),'data:',serialized.encode())
    socket.send('%d\n'.encode() % len(serialized))
    # send the serialized data
    socket.sendall(serialized.encode())
  except:
    print ('fail to send, client lost?')
    raise Exception('Failed to send to client, client dead?')

def _recv(socket):
  # read the length of the data, letter by letter until we reach EOL
  length_str = ''
  try:
    char = socket.recv(1).decode("utf-8")
  except:
    print ('receive failed')
    raise Exception('recieve failed')
  while char != '\n' and char != '':
    length_str += char
    try:
      char = socket.recv(1).decode("utf-8")
    except:
      print ('receive failed')
      raise Exception('recieve failed')

  if length_str == '':
    return None;

  total = int(length_str)
  # use a memoryview to receive the data chunk by chunk efficiently
  view = memoryview(bytearray(total))
  next_offset = 0
  while total - next_offset > 0:
    try:
      recv_size = socket.recv_into(view[next_offset:], total - next_offset)
    except:
      print ('fail to receive, node lost?')
      raise Exception('lost node')
    next_offset += recv_size
  try:
    deserialized = json.loads(view.tobytes())
  except (TypeError, ValueError):
    print ('fail to receive, json?')
    raise Exception('Data received was not in JSON format')
  return deserialized

class Worker():
        def __init__(self):
                self.host = sys.argv[1]    # The remote host
                self.port = int(sys.argv[2])   # The same port as used by the server
                self.name = sys.argv[3]
                self.hardware = sys.argv[4]
                self.socket = None
                self.startTime = 0
                self.lastEventTime = 0

                self.refreshTestRepo()

        def refreshTestRepo(self):
                if os.path.isdir('./tests') == False:
                        #os.system("git clone [gitPath] tests")
                        os.system("mkdir tests")
                else:
                        print ('refreshing repo')
                        os.chdir('./tests')
                        #os.system('git pull')
                        os.chdir('..')

        def connect(self):
                print ('connecting to ' + str(self.host) + ' on port ' + str(self.port))
                for res in socket.getaddrinfo(str(self.host), int(self.port), socket.AF_UNSPEC, socket.SOCK_STREAM):
                        af, socktype, proto, canonname, sa = res
                        try:
                                self.socket = socket.socket(af, socktype, proto)
                        except socket.error:
                                print ('socket fail')
                                self.socket = None
                                continue
                        try:
                                self.socket.connect(sa)
                        except socket.error:
                                print( 'connect fail')
                                self.socket.close()
                                self.socket = None
                                continue
                        break
                if self.socket is None:
                        print ('could not open socket')
                        return
                print ('socket success')

        def register(self):
                print ('register node to server')
                _send(self.socket,'{"type":"node","id":"'+self.name+'","hardware":"'+self.hardware+'"}')

        def listDiff(self,oldLen,lista):
                newLen = len(lista)
                if oldLen != newLen:
                        diff = newLen - oldLen
                        return newLen, lista[-diff:]
                else:
                        return oldLen, []

        def sendData(self,id,data):
                data2send = ''.join(map(str,data))
                dataLen = len(data2send)
                _send(self.socket,'{"id":"'+str(id)+'","data":"'+ data2send.encode("utf-8").hex() +'"}')

        def sendDiff(self,test):
                if len(test.powerLog):
                        self.powerLen, diff = self.listDiff(self.powerLen,test.powerLog)
                        #print(test.powerLog, 'vs', diff, ' len', self.powerLen)
                        if diff:
                                #print('send power')
                                self.sendData("powerLog",diff)
                                self.lastEventTime = time.time()
                if len(test.debugLog):
                        self.debugLen, diff = self.listDiff(self.debugLen,test.debugLog)
                        if diff:
                                #print('send debug')
                                self.sendData("debugLog",diff)
                                self.lastEventTime = time.time()
                if len(test.testTrace):
                        self.traceLen, diff = self.listDiff(self.traceLen,test.testTrace)
                        if diff:
                                #print('send trace')
                                self.sendData("traceLog",diff)
                                self.lastEventTime = time.time()

        def executeTest(self,test,firmwareName):
                print("running test",test,"on",firmwareName)
                self.startTime = time.time()
                print ("importing tests." + str(test))
                testModule = importlib.import_module("tests."+test)
                print("imported")
                reload(testModule)
                print("reloaded")
                test = testModule.Test()
                print ('running firmware ' + str(firmwareName))
                test.setFwFile(firmwareName)

                print ('will run')
                test.run()
                print(' have runned' )

                self.powerLen = 0
                self.debugLen = 0
                self.traceLen = 0
                self.lastEventTime = time.time()

                while test.testIsDone == False:
                       #print('in log loop')
                       if(time.time() - self.lastEventTime) > time_limit:
                               test.testIsDone = True
                               break
                       #print ('still alive')
                       self.sendDiff(test)
                       time.sleep(0.1)
                print('test is done')
                time.sleep(0.5)
                test.cleanUpHw()
                print('hw clean is done')
                time.sleep(0.5)
                self.sendDiff(test)
                print('diffsend done')
                time.sleep(0.5)
                self.sendData("done","success")
                print('final report done')


        def handleRequest(self):
                data = _recv(self.socket)
                #print("got: ",data)
                if data:
                        try:
                                parsed_json = json.loads(data)
                        except:
                                #print ('broken json: ' + data)
                                _send(self.socket,'{"id":"nack"}')
                                return 1

                        if parsed_json['id'] == 'runTests':
                                print ('will run test: ' + parsed_json['test'] + ' and will flash ' + parsed_json['fwName'])
                                file = open(parsed_json['fwName'],'wb')
                                file.write(bytes.fromhex(parsed_json['fwData']))
                                file.close()

                                self.executeTest(parsed_json['test'],parsed_json['fwName'])
                                return 1

                        elif parsed_json['id'] == 'refresh':
                                self.refreshTestRepo()
                                _send(self.socket,'{"id":"ack"}')
                                return 1

                        elif parsed_json['id'] == 'ping':
                                _send(self.socket,'{"id":"pong"}')
                                #print ('sent pong')
                                return 1
                        else:
                                print ('unknown: ' + data)
                                _send(self.socket,'{"id":"nack"}')
                                return 1
                else:
                        print ('Got no data, socket fail')
                        return None

        def close(self):
                print ('node socket close')
                _send(self.socket,'{"id":"quit"}')
                self.socket.close()


#worker = Worker()
#worker.connect()
#worker.register()
#while(1):
#	if worker.handleRequest() == None:
#		break
#
#sys.exit()
while(1):
	try:
		worker = Worker()
		worker.connect()
		worker.register()
		while(1):
			if worker.handleRequest() == None:
				break
	except:
		time.sleep(10)
		pass
