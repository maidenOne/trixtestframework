Trix Testing Framework
=============
A test node is a machine responsible for flashing and logging debug information on a "target".
A target is a device or development kit that we deploy code to or interact with


Test Node
=============
Currently we have two types of test nodes: Orange Pi Zero and Raspberry Pi 2 rev b.
The testnodes are using OpenOCD with gpiofs or broadcom drivers to flash the target through the SWD interface.
They are also monitoring power consumtion with INA219 modules connected to the test nodes through I2C.
Finally they are hooked up to the UART/RTT port of the targets to get debug information.

Target
=============
Se interface stubs


How to run
=============
First start the tester, its the "test manager"
cd tester
bash run.sh

Then start a node
cd node
bash run.sh

Then start the webservice
cd gui
sh run.sh

You can also start a test using command line:
cd tools
python3 runOnTestNode.py completeTest node1 test.hex
