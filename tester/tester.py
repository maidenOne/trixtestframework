import sys
import json
import os
import time
import threading
import socketserver
import glob
import queue

# For SQL integration
from testlog import TestLog
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
import datetime

engine = create_engine('sqlite:///test.db', echo=False)
TestLog.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
session = Session()
# end of SQL init

GIT_REPO = ""
script_dir = os.path.dirname(__file__) #<-- absolute dir the script is in
#LOG_PATH = os.path.abspath('./logs/') + '/'

def _send(socket, data):
  try:
    serialized = json.dumps(data)
  except (TypeError, ValueError):
    print ('fail to send, json?')
    raise Exception('You can only send JSON-serializable data')
  # send the length of the serialized data first
  try:
    socket.send('%d\n'.encode() % len(serialized))
    # send the serialized data
    socket.sendall(serialized.encode())
  except:
    print ('fail to send, client lost?')
    raise Exception('Failed to send to client, client dead?')

def _recv(socket):
  # read the length of the data, letter by letter until we reach EOL
  length_str = ''
  try:
    char = socket.recv(1).decode("utf-8")
  except:
    print ('receive failed')
    raise Exception('receive failed')
  while char != '\n' and char != '':
    length_str += char
    try:
      char = socket.recv(1).decode("utf-8")
    except:
      print ('receive failed')
      raise Exception('receive failed')

  total = int(length_str)
  # use a memoryview to receive the data chunk by chunk efficiently
  view = memoryview(bytearray(total))
  next_offset = 0
  while total - next_offset > 0:
    try:
      recv_size = socket.recv_into(view[next_offset:], total - next_offset)
    except:
      print ('fail to receive, node lost?')
      raise Exception('lost node')
    next_offset += recv_size
  try:
    deserialized = json.loads(view.tobytes())
  except (TypeError, ValueError):
    print ('fail to receive, json?')
    raise Exception('Data received was not in JSON format')
  return deserialized

class Worker():

    def __init__(self,id,hw):
        self.id = id
        self.hardware = hw
        self.todo = queue.Queue()
        self.isBusy = 0
        self.powerLog = []
        self.debugLog = []
        self.traceLog = []

        self.startTime = 0
        self.endTime = 0
        self.currentExecution = ''

class Workers():

    def __init__(self):
        self.workers = []

    def remove(self,theWorker):
        for worker in self.workers:
            if worker.id == theWorker.id:
                print ('worker removed')
                self.workers.remove(worker)
                return

    def getWorkersString(self):
        workersList = []
        for worker in self.workers:
            tests = []
            # this is not efficient, but simplifies code
            for jsonStr in list(worker.todo.queue):
                parsedJson = json.loads(jsonStr)
                tests.append(parsedJson['test'])
            workersList.append([worker.id,worker.hardware,worker.isBusy,tests,[worker.powerLog,worker.debugLog,worker.traceLog]])
        #print 'workers list: ' + str(json.dumps(workersList))
        return json.dumps(workersList)

    def register(self,worker):
        self.workers.append(worker)
        return self.workers[len(self.workers)-1].todo

class Tests():
    def __init__(self):
        self.tests = []
        self.refreshTestRepo()

    def getTestString(self):
        testList = []
        for test in self.tests:
            testList.append(test)
        return json.dumps(testList)

    def refreshTestRepo(self):
        if os.path.isdir('./tests') == False:
         #   os.system("git clone " + GIT_REPO)
           pass
        else:
            os.chdir('./tests')
            #os.system('git pull')
            listOfTests = glob.glob('*.py')
            testNames = []
            for test in listOfTests:
                testNames.append(test.split('.')[0])
            self.tests = testNames
            os.chdir('..')


    def addTests(self,test):
        self.tests.append(test)

globalWorkers = Workers()
globalTests = Tests()
globalIsRunning = True

nodePingTimer = 20

def setWorkerBusyState(workerId,state):
    for worker in globalWorkers.workers:
        if worker.id == workerId:
            worker.isBusy = state
            worker.startTime = datetime.datetime.now()

def workerAppendPower(workerId,powerLog):
    for worker in globalWorkers.workers:
        if worker.id == workerId:
            for power in powerLog:
                worker.powerLog.append(power)

def workerAppendDebug(workerId,debugLog):
    for worker in globalWorkers.workers:
        if worker.id == workerId:
            for debug in debugLog:
                worker.debugLog.append(debug)

def workerAppendTrace(workerId,traceLog):
    for worker in globalWorkers.workers:
        if worker.id == workerId:
            for trace in traceLog:
                worker.traceLog.append(trace)

def workerArchiveExecution(workerId):
    for worker in globalWorkers.workers:
        if worker.id == workerId:
            try:
                print('currentExecution:',worker.currentExecution)
                #print( 'writing archive file for ' + str(worker.id) + ' at : ' + str(LOG_PATH+str(worker.startTime)))
                #file = open(LOG_PATH+str(worker.startTime),'wb')
                log = '{ "id":"'+worker.id+'","hw":"'+worker.hardware+'","power":"'+ ''.join(worker.powerLog) +'","debug":"'+ ''.join(worker.debugLog) +'","trace":"'+ ''.join(worker.traceLog) +'","start":"'+str(worker.startTime)+'","end":"'+str(worker.endTime)+'","execution":"'+ worker.currentExecution.encode("utf-8").hex() +'"}'
                #file.write(log.encode())
                currentExec = json.loads(worker.currentExecution)
                log = TestLog(startTime=worker.startTime, testFile = currentExec['test'], firmware = currentExec['fwName'], node = worker.id, endTime=worker.endTime, hardware = worker.hardware, debugTrace = ''.join(worker.debugLog), powerTrace = ''.join(worker.powerLog), testTrace = ''.join(worker.traceLog), didPass=True)
                session.add(log)
                session.commit()
            except Exception as e:
                print ('error: ' + str(e))

            #finally:
            #    file.close()

def workerClearLogs(workerId):
    for worker in globalWorkers.workers:
        if worker.id == workerId:
            worker.traceLog = []
            worker.debugLog = []
            worker.powerLog = []
            worker.isBusy = 0
#{"type":"request","id":"runTests","target":"test","test":"powerOnPowerOff","fwName":"coreB01.bin","fwData":

def find_second_last(text, pattern):
    return text.rfind(pattern, 0, text.rfind(pattern))

def getLogs():
    fileList = []

    engine2 = create_engine('sqlite:///test.db', echo=False)
    Session2 = sessionmaker(bind=engine2)
    session2 = Session()

    query = session2.query(TestLog)
    for testlog in query:
        print(testlog)
        time_difference = testlog.endTime - testlog.startTime
        time_difference_in_seconds = time_difference / datetime.timedelta(seconds=1)
        fileList.append([testlog.startTime.isoformat(),testlog.didPass,testlog.node,testlog.hardware,testlog.testFile,testlog.firmware,time_difference_in_seconds,testlog.id])

    return json.dumps(fileList,default=str)

def getRawLog(filename):
    engine2 = create_engine('sqlite:///test.db', echo=False)
    Session2 = sessionmaker(bind=engine2)
    session2 = Session()
    testlog = session2.query(TestLog).filter_by(id=filename).first()

    print(testlog)
    time_difference = testlog.endTime - testlog.startTime
    time_difference_in_seconds = time_difference / datetime.timedelta(seconds=1)
    testlog.startTime = testlog.startTime.isoformat()
    testlog.endTime = testlog.endTime.isoformat()
    return json.dumps({'id':testlog.id, 'startTime':testlog.startTime, 'endTime':testlog.endTime, 'deltaTime':time_difference_in_seconds, 'firmware':testlog.firmware, 'testFile':testlog.testFile, 'powerTrace':testlog.powerTrace, 'debugTrace':testlog.debugTrace, 'testTrace':testlog.testTrace, 'node':testlog.node, 'hardware':testlog.hardware, 'didPass':testlog.didPass})

def deleteLog(filename):
    if filename:
        try:
            engine2 = create_engine('sqlite:///test.db', echo=False)
            Session2 = sessionmaker(bind=engine2)
            session2 = Session()
            testlog = session2.query(TestLog).filter_by(id=filename).first()
            print('delete',testlog)
            session2.delete(testlog)
            session2.commit()
            return '{"id":"success"}'
        except:
            return '{"id":"fail","data":"no such file: '+str(filename)+'"}'
    return '{"id":"fail","data":"no file provided"}'

class ThreadedTCPRequestHandler(socketserver.BaseRequestHandler):

    def sendWorkerRequest(self):
        target = self.parsed_json['target']
        for worker in globalWorkers.workers:
            if worker.id == target or target == 'ALL':
                worker.todo.put(self.data);

    def handleRequest(self):
        requestId = self.parsed_json['id']
        print('gotRequest',requestId)
        if requestId == 'getNodes':
            _send(self.request,globalWorkers.getWorkersString())
        elif requestId == 'getTests':
            _send(self.request,globalTests.getTestString())
        elif requestId == 'getLogs':
            _send(self.request,getLogs())
        elif requestId == 'refreshTests':
            globalTests.refreshTestRepo()
            _send(self.request,globalTests.getTestString())
        elif requestId == 'refresh':
            _send(self.request,'{"id":"ack"}')
            globalTests.refreshTestRepo()
            self.sendWorkerRequest()
        elif requestId == 'getLogFile':
            _send(self.request,getRawLog(self.parsed_json['file']))
        elif requestId == 'deleteLogFile':
            _send(self.request,deleteLog(self.parsed_json['file']))
        elif requestId == 'runTests':
            _send(self.request,'{"id":"ack"}')
            self.sendWorkerRequest()
        else:
            _send(self.request,'{"id":"nack","data":"unknown command"}')

    def handleNode(self,id,hardware):
            worker = Worker(id,hardware)
            todo = globalWorkers.register(worker)
            while(globalIsRunning):
                try:
                    worker.currentExecution = todo.get(block=True,timeout=nodePingTimer)
                    setWorkerBusyState(worker.id,1)
                    print ("send ping")
                    try:
                        _send(self.request,worker.currentExecution)
                    except:
                        print (worker.id + " died, failed to send")
                        globalWorkers.remove(worker)
                        return
                    isDone = 0
                    while(isDone != 1):
                        try:
                            self.data = _recv(self.request)
                        except:
                            print (worker.id + " died, failed to receive")
                            globalWorkers.remove(worker)
                            return
                        if self.data:
                            self.parsed_json = json.loads(self.data)
                            #print('got node data', self.data, 'vs', self.parsed_json)
                            if self.parsed_json['id'] == 'done':
                                print (str(worker.id) + " is done, Archive!")
                                isDone = 1
                                worker.endTime = datetime.datetime.now()
                                workerArchiveExecution(worker.id)
                                workerClearLogs(worker.id)
                            else:
                                data = self.parsed_json['data']
                                if self.parsed_json['id'] == 'traceLog':
                                    #print (str(worker.id) + " traceLog")
                                    workerAppendTrace(worker.id,data)
                                if self.parsed_json['id'] == 'powerLog':
                                    #print (str(worker.id) + " powerLog")
                                    workerAppendPower(worker.id,data)
                                if self.parsed_json['id'] == 'debugLog':
                                    #print (str(worker.id) + " debugLog")
                                    workerAppendDebug(worker.id,data)
                        else:
                            print ('got nothing after request, will quit')
                            globalWorkers.remove(worker)
                            return

                except Exception as ex:
                    print(ex)
                    setWorkerBusyState(worker.id,0)
                    print (worker.id + " is idle")
                    self.request.settimeout(nodePingTimer)
                    try:
                        _send(self.request,'{"id":"ping"}')
                    except:
                        print (worker.id + " died, failed to send")
                        globalWorkers.remove(worker)
                        return
                    try:
                        self.data = _recv(self.request)
                        self.parsed_json = json.loads(self.data)
                    except:
                        print (worker.id + " died, failed to send")
                        globalWorkers.remove(worker)
                        return

                    if self.parsed_json['id'] != 'pong':
                        print (worker.id + " died, ping timeout")
                        globalWorkers.remove(worker)
                        return
                    self.request.settimeout(None)

    def handle(self):
        # self.request is the TCP socket connected to the client
        self.data = _recv(self.request)
        self.parsed_json = json.loads(self.data)
        #print("got data:",self.parsed_json)
        if str(self.parsed_json['type']) == 'request':
            self.handleRequest()
        elif str(self.parsed_json['type']) == 'node':
            self.handleNode(self.parsed_json['id'],self.parsed_json['hardware'])

class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass

class Tester():
    def __init__(self):
        # Port 0 means to select an arbitrary unused port
        HOST, PORT = "0.0.0.0", int(sys.argv[1])

        self.server = ThreadedTCPServer((HOST, PORT), ThreadedTCPRequestHandler,False)
        self.server.allow_reuse_address = True
        self.server.server_bind()
        self.server.server_activate()
        ip, port = self.server.server_address

        # Start a thread with the server -- that thread will then start one
        # more thread for each request
        self.server_thread = threading.Thread(target=self.server.serve_forever)
        # Exit the server thread when the main thread terminates
        self.server_thread.daemon = True
        self.server_thread.start()
        print ("Server loop running in thread:", self.server_thread.name)

    def setPingTimer(self, value):
        global nodePingTimer
        nodePingTimer = value

    def stop(self):
        print ('server is stopping')
        global globalIsRunning
        globalIsRunning = False
        self.server.shutdown()
        self.server.server_close()
        self.server_thread.join()
        print ('server have ended')


#if not os.path.exists(LOG_PATH):
#    os.makedirs(LOG_PATH)

test = Tester()
while(1):
    #print('still alive')
    time.sleep(10)
test.stop()
