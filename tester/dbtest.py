from testlog import TestLog

from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
import datetime

engine = create_engine('sqlite:///test.db', echo=False)

TestLog.metadata.create_all(engine)

Session = sessionmaker(bind=engine)
session = Session()

log = TestLog(didPass = True, startTime=datetime.datetime.now(), hardware = "pinetime", node = "maidenNode 1")
session.add(log)
log = TestLog(didPass = False, startTime=datetime.datetime.now(), hardware = "pinetime", node = "maidenNode 1")
session.add(log)
session.commit()

print(log.hardware, log.node, log.didPass)

query = session.query(TestLog).filter_by(didPass=True)
#print(query)
print(query.count())
