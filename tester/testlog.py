from sqlalchemy import Column, Integer, String, Boolean, DateTime
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class TestLog(Base):
    __tablename__ = 'TestLog'

    id = Column(Integer, primary_key=True)
    startTime = Column(DateTime)
    endTime = Column(DateTime)
    firmware = Column(String)
    testFile = Column(String)
    powerTrace = Column(String)
    debugTrace = Column(String)
    testTrace = Column(String)
    node = Column(String)
    hardware = Column(String)
    didPass = Column(Boolean)
