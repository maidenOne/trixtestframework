#!/usr/bin/env python

import logging
import tornado.escape
import tornado.ioloop
import tornado.web
import os.path
import uuid
import sys
import time

import errno
import functools
import socket

import tornado.ioloop
from tornado.iostream import IOStream

sys.path.append("../")
import TrixAPI

from tornado.concurrent import Future
from tornado import gen
from tornado.options import define, options, parse_command_line

def cleanListFromHex(data):
        data = bytes.fromhex(data).decode('utf-8')
        data = data.replace('] [','],[')
        data = data.replace('][','],[')
        data = data.replace(', b',',')
        #data = '[' + str(data) + ']'
        data = ''.join(['[',data,']'])
        return eval(data)

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        print("got get")
        api = TrixAPI.TrixAPI()
        nodes = api.getNodes()
        tests = api.getTests()
        self.render("home.html",items=nodes,tests=tests)

    def post(self):
        print("got post")
        api = TrixAPI.TrixAPI()
        try:
            print("upload")
            fileinfo = self.request.files['fileToUpload'][0]
            filename = fileinfo['filename']
            print("filename:",filename)
            body = fileinfo['body']
            encodedBody = body.hex()
            print("run test")
            api.runTestOnNodeWithFwHex(self.get_body_argument("test"), self.get_body_argument("node"), filename, encodedBody)
        except Exception as e:
            print(e)
            print("no file uploaded")
        stuff = api.getNodes()
        tests = api.getTests()
        self.render("home.html",items=stuff,tests=tests)

class NodeHandler(tornado.web.RequestHandler):
    def get(self):
        api = TrixAPI.TrixAPI()
        nodes = api.getNodes()
        tests = api.getTests()
        self.render("nodes.html",items=nodes,tests=tests)

class HistoryHandler(tornado.web.RequestHandler):
    def get(self):
        api = TrixAPI.TrixAPI()
        logs = api.getLogs()
        self.render("history.html",logs=logs,time=time)

class HistoryCompareHandler(tornado.web.RequestHandler):
    def get(self):
        log = self.get_argument('log')
        api = TrixAPI.TrixAPI()
        logs = api.getLogs()
        self.render("historyCompare.html",log=log, logs=logs,time=time)

class HistoryLogHandler(tornado.web.RequestHandler):
    def get(self):
        #try:
        filename = self.get_argument('log')
        api = TrixAPI.TrixAPI()
        log = api.getLog(filename)
        info =  [log['startTime'],log['hardware'],log['node'],log['deltaTime']]
        trace = cleanListFromHex(log['testTrace'])
        debug = cleanListFromHex(log['debugTrace'])
        power = cleanListFromHex(log['powerTrace'])
        id = log['id']
        newPower = []
        try:
            oldM = float(power[0][0])
        except:
            oldM = 0
        for m in power:
            newPower.append([round(float(m[0])-oldM,1),m[1]])

        self.render("historyLog.html",time=time, info=info, trace=trace,debug=debug,power=newPower,id=id)
       # except:
       #     print "no log file named " + str(filename)

class HistoryLogsHandler(tornado.web.RequestHandler):
    def get(self):
        #try:
        filename = self.get_argument('log')
        api = TrixAPI.TrixAPI()
        log = api.getLog(filename)
        info =  [log['startTime'],log['hardware'],log['node'],log['deltaTime']]
        trace = cleanListFromHex(log['testTrace'])
        debug = cleanListFromHex(log['debugTrace'])
        power = cleanListFromHex(log['powerTrace'])

        newPower = []
        try:
            oldM = float(power[0][0])
        except:
            oldM = 0
        for m in power:
            newPower.append([round(float(m[0])-oldM,1),m[1]])

        filename2 = self.get_argument('log2')
        log2 = api.getLog(filename2)
        info2 =  [log2['startTime'],log2['hardware'],log2['node'],log2['deltaTime']]
        trace2 = cleanListFromHex(log2['testTrace'])
        debug2 = cleanListFromHex(log2['debugTrace'])
        power2 = cleanListFromHex(log2['powerTrace'])

        newPower2 = []
        try:
            oldM2 = float(power2[0][0])
        except:
            oldM2 = 0
        for m in power2:
            newPower2.append([round(float(m[0])-oldM2,1),m[1]])

        self.render("historyLog2.html",time=time, info=info, trace=trace,debug=debug,power=newPower, info2=info2, trace2=trace2,debug2=debug2,power2=newPower2)
       # except:
       #     print "no log file named " + str(filename)

class HistoryDeleteHandler(tornado.web.RequestHandler):
    def get(self):
        #try:
        filename = self.get_argument('log')
        api = TrixAPI.TrixAPI()
        api.deleteLog(filename)
       # except:
       #     print "no log file named " + str(filename)
        stuff = api.getNodes()
        tests = api.getTests()
        self.render("home.html",items=stuff,tests=tests)

class MessageNewHandler(tornado.web.RequestHandler):
    def post(self):
        message = {
            "id": str(uuid.uuid4()),
            "body": self.get_argument("body"),
        }
        # to_basestring is necessary for Python 3's json encoder,
        # which doesn't accept byte strings.
        message["html"] = tornado.escape.to_basestring(
            self.render_string("message.html", message=message))
        if self.get_argument("next", None):
            self.redirect(self.get_argument("next"))
        else:
            self.write(message)
        global_message_buffer.new_messages([message])


class FormHandler(tornado.web.RequestHandler):
    def get(self):
        api = TrixAPI.TrixAPI()
        stuff = api.getNodes()
        tests = api.getTests()
        self.render("form.html",items=stuff,tests=tests)


class TestHandler(tornado.web.RequestHandler):

    def get(self):
        self.render("test.html")

    def post(self):
        self.set_header("Content-Type", "text/plain")
        self.write("You wrote " + self.get_body_argument("message"))

class RefreshHandler(tornado.web.RequestHandler):

    def get(self):
        api = TrixAPI.TrixAPI()
        api.refresh()
        self.redirect('/')

def main():
    print ("reload")
    #reload(sys)
    #sys.setdefaultencoding('utf8')

    parse_command_line()
    app = tornado.web.Application(
        [
            (r"/", MainHandler),
            (r"/form", FormHandler),
            (r"/refresh", RefreshHandler),
            (r"/test", TestHandler),
            (r"/nodes",NodeHandler),
            (r"/history",HistoryHandler),
            (r"/historyCompare",HistoryCompareHandler),
            (r"/historyDelete",HistoryDeleteHandler),
            (r"/historyLog",HistoryLogHandler),
            (r"/historyLogs",HistoryLogsHandler),
            ],
        page_title=u"Trix Test Interface",
        cookie_secret="__TODO:_GENERATE_YOUR_OWN_RANDOM_VALUE_HERE__",
        template_path=os.path.join(os.path.dirname(__file__), "templates"),
        static_path=os.path.join(os.path.dirname(__file__), "static"),
        xsrf_cookies=False,
        debug=True,
        )
    print ("start")
    app.listen(8080)
    tornado.ioloop.IOLoop.current().start()
    print ("start")



if __name__ == "__main__":
    main()
