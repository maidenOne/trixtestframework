import os
import sys

sys.path.insert(1, os.path.join(sys.path[0], '..'))

import TrixAPI

class bcolors:
	HEADER = '\033[95m'
	BLUE = '\033[94m'
	GREEN = '\033[92m'
	WARNING = '\033[93m'
	RED = '\033[91m'
	END = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'

api = TrixAPI.TrixAPI(host='localhost')

def post(testName,node,testFile):
	with open(testFile, 'r') as fp:
		data = fp.read()
		api.runTestOnNodeFilePath(testName, node, testFile)

if len(sys.argv) < 3:
	print (bcolors.RED + "Too few args" + bcolors.END)
	print (str(sys.argv[0]) + " TestName NodeName TestFile")
	
	tests = api.getTests()
	nodes = api.getNodes()
	logs = api.getLogs()
	print (bcolors.BOLD + "Tests: " + bcolors.END)
	for test in tests:
		print (str(test))
	print (bcolors.BOLD + "Nodes: ")
	print ("Name  Hardware" + bcolors.END)
	for node in nodes:
		print (node[0] + " " + node[1])

	print (bcolors.BOLD + "Logs: ")
	print ("LogName  Hardware Node Test" + bcolors.END)
	logs = logs[:10]
	for log in logs:
		if "SUCCESS" in log[1]:
			print (bcolors.GREEN + log[0] + " " + log[2] + " " + log[3] + " " + log[4] + bcolors.END)
		else:
			print (bcolors.RED + log[0] + " " + log[2] + " " + log[3] + " " + log[4] + bcolors.END)

else:
	post(sys.argv[1],sys.argv[2],sys.argv[3])
