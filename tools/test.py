import sys,os
import curses

sys.path.insert(1, os.path.join(sys.path[0], '..'))

import TrixAPI
import sys
import os
import time

def renderLine(stdscr,x,y,text,color,maxWidth):
	try:
		text = text[0:maxWidth]
		stdscr.addstr(y, x, str(text), color)
	except:
		return
	stdscr.move(0, 0)

def renderText(stdscr,x,y,text,color,maxWidth):
    lineCount = 0
    if len(text):
        for line in reversed(text):
            try:
                line = str(line[1])[0:maxWidth]
                stdscr.addstr(y + lineCount, x, str(line), color)
            except:
                return
            stdscr.move(0, 0)
            lineCount+=1

def draw_menu(stdscr):
	k = 0
	cursor_x = 0
	cursor_y = 0

	# Clear and refresh the screen for a blank canvas
	stdscr.clear()
	stdscr.refresh()
	stdscr.nodelay(1)

	# Start colors in curses
	curses.start_color()
	curses.init_pair(1, curses.COLOR_CYAN, curses.COLOR_BLACK)
	curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK)
	curses.init_pair(3, curses.COLOR_GREEN, curses.COLOR_BLACK)
	curses.init_pair(4, curses.COLOR_BLACK, curses.COLOR_WHITE)

	api = TrixAPI.TrixAPI(host='localhost')


	with open(sys.argv[3], 'r') as fp:
		data = fp.read()
		api.runTestOnNodeFilePath(sys.argv[1],sys.argv[2],sys.argv[3])
	# Loop where k is the last character pressed
	while (k != ord('q')):

		# Initialization
		stdscr.clear()
		height, width = stdscr.getmaxyx()

	if k == curses.KEY_DOWN:
		cursor_y = cursor_y + 1
	elif k == curses.KEY_UP:
		cursor_y = cursor_y - 1
	elif k == curses.KEY_RIGHT:
		cursor_x = cursor_x + 1
	elif k == curses.KEY_LEFT:
		cursor_x = cursor_x - 1

	cursor_x = max(0, cursor_x)
	cursor_x = min(width-1, cursor_x)

	cursor_y = max(0, cursor_y)
	cursor_y = min(height-1, cursor_y)

	nodes = api.getNodes()
	numNodes = len(nodes)
	if(numNodes):
		node1_name = str(nodes[0][0]) + " perding: " + str(','.join(nodes[0][3]))
		node1_log = nodes[0][4][1]
		node1_power = nodes[0][4][0]
	if(numNodes > 1):
		node2_name = str(nodes[1][0]) + " pending: " + str(','.join(nodes[1][3]))
		node2_log = nodes[1][4][1]
		node2_power = nodes[1][4][0]

        #node1_name = "flasher1"
        #node1_log = [[1532677305.74627, u'OK'], [1532677305.847371, u'OK'], [1532677305.948063, u'OK'], [1532677306.048435, u'OK'], [1532677306.149838, u'+CME ERROR: unknown'], [1532677306.249893, u'240080007641069'], [1532677306.257084, u'OK'], [1532677306.352044, u'359315078949145'], [1532677306.358053, u'OK'], [1532677306.454438, u'+CME ERROR: operation not allowed'], [1532677307.554339, u'OK'], [1532677307.655243, u'OK'], [1532677307.756687, u'+CME ERROR: operation not allowed'], [1532677307.85519, u'+CREG: 1,0'], [1532677307.863072, u'OK'], [1532677308.955731, u'+CREG: 1,0'], [1532677308.957598, u'OK'], [1532677310.058073, u'+CREG: 1,0'], [1532677310.069218, u'OK'], [1532677311.160216, u'+CREG: 1,0'], [1532677311.167694, u'OK'], [1532677312.261733, u'+CREG: 1,0'], [1532677312.270844, u'OK']]
        #node1_power = [[1532676265.562687,1], [1532676265.563394, 2], [1532676265.564474, 3]]
        #node2_name = "flasher2"
        #node2_log = [[1532676265.562687, u'AT'], [1532676265.563394, u'ATE0'], [1532676265.564474, u'AT+UGIND=1']]
        #node2_power = [[1532676265.562687,1], [1532676265.563394, 2], [1532676265.564474, 3]]

        # Centering calculations
	start_y = 2
	boxleft_x = 1
	boxright_x = int((width // 2))
	maxWidth = int((width-8)//2)

	# Rendering some text
	whstr = "press q to quit"
	stdscr.addstr(0, 0, whstr, curses.color_pair(3))

        #print left box
	if(numNodes > 1):
		renderLine(stdscr,boxleft_x,start_y-1,node1_name,curses.color_pair(1),maxWidth)
		renderText(stdscr,boxleft_x,start_y,node1_power,curses.color_pair(1),maxWidth)
		renderText(stdscr,boxleft_x+4,start_y,node1_log,curses.color_pair(1),maxWidth)
	if(numNodes > 1):
		renderLine(stdscr,boxright_x,start_y-1,node2_name,curses.color_pair(2),maxWidth)
		renderText(stdscr,boxright_x,start_y,node2_power,curses.color_pair(2),maxWidth)
		renderText(stdscr,boxright_x+4,start_y,node2_log,curses.color_pair(2),maxWidth)

        # Refresh the screen
	stdscr.refresh()

	for i in range(1,10):
		# Wait for next input
		k = stdscr.getch()
		if k == ord('q'):
			return
		time.sleep(0.1)

def main():
	curses.wrapper(draw_menu)

if __name__ == "__main__":
	main()
